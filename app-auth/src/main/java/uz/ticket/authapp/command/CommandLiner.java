package uz.ticket.authapp.command;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import uz.ticket.authapp.entity.Role;
import uz.ticket.authapp.entity.User;
import uz.ticket.authapp.entity.enums.Authorities;
import uz.ticket.authapp.repository.RoleRepository;
import uz.ticket.authapp.repository.UserRepository;

import java.util.Arrays;

@RequiredArgsConstructor
@Component
public class CommandLiner implements CommandLineRunner {

    final RoleRepository roleRepository;
    final UserRepository userRepository;

    @Value("${command.init-mode}")
    Boolean check;

    final PasswordEncoder passwordEncoder;

    @Override
    public void run(String... args) throws Exception {

        Role userRole=new Role();
        userRole.setRoleName("USER");
        userRole.setAuthorities(
                Arrays.asList(
                        Authorities.BOOK_TABLE,
                        Authorities.CANCLE_OWN_BOOKING,
                        Authorities.SHOW_RESTAURANT,
                        Authorities.SHOW_TABLE
                )
        );
        roleRepository.save(userRole);

        if (check) {

            Role admin=new Role();
            admin.setRoleName("ADMIN");
            admin.setAuthorities(
                    Arrays.asList(
                            Authorities.EDIT_RESTAURANT,
                            Authorities.ADD_RESTAURANT,
                            Authorities.SHOW_RESTAURANT,
                            Authorities.DELETE_RESTAURANT,
                            Authorities.EDIT_TABLE,
                            Authorities.ADD_TABLE,
                            Authorities.SHOW_TABLE,
                            Authorities.DELETE_TABLE,
                            Authorities.BOOK_TABLE,
                            Authorities.CANCLE_BOOKING,
                            Authorities.CANCLE_OWN_BOOKING,
                            Authorities.ADD_ROLE,
                            Authorities.EDIT_ROLE,
                            Authorities.DELETE_ROLE,
                            Authorities.SHOW_ROLE
                    )
            );
            Role moder=new Role();
            moder.setRoleName("MODERATOR");
            moder.setAuthorities(
                    Arrays.asList(
                            Authorities.SHOW_RESTAURANT,
                            Authorities.EDIT_TABLE,
                            Authorities.ADD_TABLE,
                            Authorities.DELETE_TABLE,
                            Authorities.SHOW_TABLE,
                            Authorities.CANCLE_BOOKING
                    )
            );

            roleRepository.save(admin);
            roleRepository.save(moder);
            User moder1 = new User();
            moder1.setEmail("moder@gmail.com");
            moder1.setName("moder");
            moder1.setPassword(passwordEncoder.encode("moder"));
            moder1.setRole(moder);
            User user = new User();
            user.setEmail("admin@gmail.com");
            user.setName("admin");
            user.setPassword(passwordEncoder.encode("admin"));
            user.setRole(admin);


            userRepository.save(moder1);
            userRepository.save(user);

        }
    }
}
