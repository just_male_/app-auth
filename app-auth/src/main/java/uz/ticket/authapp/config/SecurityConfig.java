package uz.ticket.authapp.config;

import lombok.RequiredArgsConstructor;
import org.aspectj.weaver.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.client.RestTemplate;
import uz.ticket.authapp.entity.User;
import uz.ticket.authapp.entity.enums.Authorities;
import uz.ticket.authapp.filter.MyFilter;
import uz.ticket.authapp.repository.UserRepository;
import uz.ticket.authapp.util.Utills;

import java.util.Optional;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor

public class SecurityConfig {
    final UserRepository userRepository;


    final MyFilter myFilter;
    @Bean
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        return http
                .csrf()
                .disable()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeHttpRequests(
                        auth -> {
                            auth
                                    .requestMatchers("/api/auth/info/**").hasAnyAuthority(Authorities.SHOW_RESTAURANT.name())
                                    .requestMatchers("/api/restaurant/add").hasAnyAuthority(Authorities.ADD_RESTAURANT.name())
                                    .requestMatchers("/api/restaurant/edit/**").hasAnyAuthority(Authorities.EDIT_RESTAURANT.name())
                                    .requestMatchers("/api/restaurant/delete/**").hasAnyAuthority(Authorities.DELETE_RESTAURANT.name())
                                    .requestMatchers("/api/restaurant/show/**").hasAnyAuthority(Authorities.SHOW_RESTAURANT.name())
                                    .requestMatchers("/api/restaurant/show").hasAnyAuthority(Authorities.SHOW_RESTAURANT.name())
                                    .requestMatchers("/api/table/add").hasAnyAuthority(Authorities.ADD_TABLE.name())
                                    .requestMatchers("/api/table/edit/**").hasAnyAuthority(Authorities.EDIT_TABLE.name())
                                    .requestMatchers("/api/table/show/**").hasAnyAuthority(Authorities.SHOW_TABLE.name())
                                    .requestMatchers("/api/table/show").hasAnyAuthority(Authorities.SHOW_TABLE.name())
                                    .requestMatchers("/api/table/delete/**").hasAnyAuthority(Authorities.DELETE_TABLE.name())
                                    .requestMatchers("/api/role/add").hasAnyAuthority(Authorities.ADD_ROLE.name())
                                    .requestMatchers("/api/role/show/**").hasAnyAuthority(Authorities.SHOW_ROLE.name())
                                    .requestMatchers("/api/role/show").hasAnyAuthority(Authorities.SHOW_ROLE.name())
                                    .requestMatchers("/api/role/edit/**").hasAnyAuthority(Authorities.EDIT_ROLE.name())
                                    .requestMatchers("/api/role/delete/**").hasAnyAuthority(Authorities.DELETE_ROLE.name())

                                    .requestMatchers(Utills.OPEN_PAGES)
                                    .permitAll()
                                    .anyRequest()
                                    .authenticated();
                        }
                )
                .authenticationProvider(authenticationProvider())
                .addFilterBefore(myFilter, UsernamePasswordAuthenticationFilter.class)
                .build();


    }

    @Bean
    public AuthenticationProvider authenticationProvider() {
        final DaoAuthenticationProvider provider = new DaoAuthenticationProvider();
        provider.setPasswordEncoder(passwordEncoder());
        provider.setUserDetailsService(userDetailsService());
        return provider;
    }


    @Bean
    public UserDetailsService userDetailsService() {
        return new UserDetailsService() {
            @Override
            public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
                User user = userRepository.findByEmail(email).orElseThrow(() -> new UsernameNotFoundException("user not found with " + email));
                return user;
            }
        };
    }

    @Bean
    public AuthenticationManager authenticationManager(AuthenticationConfiguration config) throws Exception {
        return config.getAuthenticationManager();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
