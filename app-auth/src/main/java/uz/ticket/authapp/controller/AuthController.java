package uz.ticket.authapp.controller;

import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.*;
import uz.ticket.authapp.payload.UserDTO;

import java.util.UUID;


@RequestMapping("/api/auth")
public interface AuthController {
    @GetMapping("/info")
    HttpEntity<?> getInfo();
    @PostMapping("/signUp")
    HttpEntity<?> signUp(@RequestBody UserDTO userDTO);
    @PostMapping("/signIn")
    HttpEntity<?> signIn(@RequestBody UserDTO userDTO);
    @PostMapping("/signIn/basic")
    HttpEntity<?> signInBasic(@RequestBody UserDTO userDTO);

    @GetMapping("/check")
    HttpEntity<?> confirmAccount(@RequestParam("userId")UUID userId,
                                 @RequestParam("code") String  code);

}
