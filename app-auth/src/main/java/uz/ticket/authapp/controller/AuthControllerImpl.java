package uz.ticket.authapp.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import uz.ticket.authapp.command.CommandLiner;
import uz.ticket.authapp.entity.Role;
import uz.ticket.authapp.entity.User;
import uz.ticket.authapp.entity.enums.Authorities;
import uz.ticket.authapp.exception.BadRequestException;
import uz.ticket.authapp.filter.JwtProvider;
import uz.ticket.authapp.payload.UserDTO;
import uz.ticket.authapp.repository.RoleRepository;
import uz.ticket.authapp.repository.UserRepository;
import uz.ticket.authapp.service.EmailDetails;
import uz.ticket.authapp.service.EmailService;
import uz.ticket.authapp.service.RoleService;
import uz.ticket.authapp.util.Utills;

import java.util.*;

@RestController
@RequiredArgsConstructor
public class AuthControllerImpl implements AuthController {



    final UserRepository userRepository;
    final JwtProvider jwtProvider;
    final PasswordEncoder passwordEncoder;
    final UserDetailsService userDetailsService;
    final EmailService emailService;
    final RoleService roleService;
    final RoleRepository roleRepository;

    @Override
    public HttpEntity<?> getInfo() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = null;
        if (Objects.nonNull(authentication)) {
            user = (User) authentication.getPrincipal();
        }

        return ResponseEntity.ok("Salom " + user);
    }

    @Override
    public HttpEntity<?> signUp(UserDTO userDTO) {

        User user = new User();
        user.setEmail(userDTO.getEmail());
        user.setName(userDTO.getName());
        user.setPassword(passwordEncoder.encode(userDTO.getPassword()));
        user.setCode("123");

        Optional<Role> role = roleRepository.findByRoleName("USER");
        user.setRole(role.get());
        userRepository.save(user);
        String emailPath = "userId=" + user.getId() + "&code=" + user.getCode();

        emailService.sendSimpleMail(
                EmailDetails.builder()
                        .msgBody("http://localhost:8080/api/auth/check?" + emailPath)
                        .subject("Account confirmation email")
                        .recipient(userDTO.getEmail())
                        .build()
        );
        return ResponseEntity.ok("saqlandi");
    }

    @Override
    public HttpEntity<?> signIn(UserDTO userDTO) {

        String token = "";
        User user = (User) userDetailsService.loadUserByUsername(userDTO.getEmail());
        if (!user.isEnabled())
            throw new BadRequestException("please confirm your account");
        if (passwordEncoder.matches(userDTO.getPassword(), user.getPassword())) {
            token = jwtProvider.generateJson(user.getEmail());
            return ResponseEntity.ok(Utills.BEARER_AUTH + token);
        }
        throw new RuntimeException("password not matched");
    }

    @Override
    public HttpEntity<?> signInBasic(UserDTO userDTO) {
        User user = userRepository.findByEmailAndPassword(userDTO.getEmail(), userDTO.getPassword()).orElseThrow();
        Base64.Encoder encoder = Base64.getEncoder();
        String basicToken = user.getEmail() + ":" + user.getPassword();
        String token = new String(encoder.encode(basicToken.getBytes()));
        System.out.println(token = Utills.BASIC_AUTH + token);
        return ResponseEntity.ok(token);
    }

    @Override
    public HttpEntity<?> confirmAccount(UUID userId, String code) {
        User user = userRepository.findById(userId).orElseThrow(() -> new BadRequestException("wrong info"));
        if (Objects.equals(user.getCode(), code))
            user.setEnabled(true);
        userRepository.save(user);
        return user.isEnabled()?ResponseEntity.ok("user enabled"):
                ResponseEntity.status(HttpStatus.CONFLICT).body("something went wrong");

    }

}
