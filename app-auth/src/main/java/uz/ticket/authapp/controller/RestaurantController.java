package uz.ticket.authapp.controller;


import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.ticket.authapp.entity.Restaurant;
import uz.ticket.authapp.payload.RestaurantDTO;

@RequestMapping("/api/restaurant")
public interface RestaurantController {

    @PostMapping("/add")
    HttpEntity<?> add(@RequestBody RestaurantDTO restaurantDTO);
    @GetMapping("/show/{id}")
    HttpEntity<?> show(@PathVariable Integer id);
    @GetMapping()
    HttpEntity<?> showAll();
    @PutMapping("/edit/{id}")
    HttpEntity<?> edit(@PathVariable Integer id,
                       @RequestBody RestaurantDTO restaurantDTO);
    @DeleteMapping("/delete/{id}")
    HttpEntity<?> delete(@PathVariable() Integer id);

}
