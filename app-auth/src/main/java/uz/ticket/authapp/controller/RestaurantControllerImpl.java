package uz.ticket.authapp.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import uz.ticket.authapp.entity.Restaurant;
import uz.ticket.authapp.payload.RestaurantDTO;
import uz.ticket.authapp.service.RestaurantService;

@RestController
@RequiredArgsConstructor
public class RestaurantControllerImpl implements RestaurantController{
    RestaurantService restaurantService;
    @Override
    public HttpEntity<?> add(RestaurantDTO restaurantDTO) {
        return ResponseEntity.ok(restaurantService.add(restaurantDTO));
    }

    @Override
    public HttpEntity<?> show(Integer id) {
        return ResponseEntity.ok(restaurantService.show(id));
    }

    @Override
    public HttpEntity<?> showAll() {
        return ResponseEntity.ok(restaurantService.showAll());
    }

    @Override
    public HttpEntity<?> edit(Integer id, RestaurantDTO restaurantDTO) {
        return ResponseEntity.ok(restaurantService.edit(id,restaurantDTO));
    }

    @Override
    public HttpEntity<?> delete(Integer id) {
        return ResponseEntity.ok(restaurantService.delete(id));
    }
}
