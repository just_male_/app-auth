package uz.ticket.authapp.controller;

import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.*;
import uz.ticket.authapp.entity.Role;
import uz.ticket.authapp.payload.RoleDTO;

@RequestMapping("/api/role")
public interface RoleController {
    @PostMapping("/add")
    HttpEntity<?> add(@RequestBody RoleDTO role);
    @GetMapping("/get/{id}")
    HttpEntity<?> get(@PathVariable Integer id);
    @GetMapping
    HttpEntity<?> getAll();
    @PutMapping("/edit/{id}")
    HttpEntity<?> edit(@PathVariable Integer id,@RequestBody RoleDTO role);

    @DeleteMapping("/delete/{id}")
    HttpEntity<?> delete(@PathVariable Integer id);
}
