package uz.ticket.authapp.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import uz.ticket.authapp.entity.Role;
import uz.ticket.authapp.payload.RoleDTO;
import uz.ticket.authapp.service.RoleService;

@RestController
@RequiredArgsConstructor
public class RoleControllerImpl implements RoleController{
    final RoleService roleService;
    @Override
    public HttpEntity<?> add(RoleDTO roleDTO) {
        return ResponseEntity.ok(roleService.add(roleDTO));
    }

    @Override
    public HttpEntity<?> get(Integer id) {
        return ResponseEntity.ok(roleService.get(id));
    }

    @Override
    public HttpEntity<?> getAll() {
        return ResponseEntity.ok(roleService.getAll());
    }

    @Override
    public HttpEntity<?> edit(Integer id, RoleDTO roleDTO) {
        return ResponseEntity.ok(roleService.edit(id,roleDTO));
    }

    @Override
    public HttpEntity<?> delete(Integer id) {
        return ResponseEntity.ok(roleService.delete(id));
    }
}
