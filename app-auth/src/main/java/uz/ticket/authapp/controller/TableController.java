package uz.ticket.authapp.controller;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import uz.ticket.authapp.payload.RoleDTO;
import uz.ticket.authapp.payload.TableDTO;

import java.net.http.HttpClient;
@RequestMapping("/api/table")
public interface TableController {
    @PostMapping("/add")
    HttpEntity<?> add(@RequestBody TableDTO tableDTO);
    @GetMapping("/get/{id}")
    HttpEntity<?> get(@PathVariable Integer id);
    @GetMapping
    HttpEntity<?> getAll();
    @PutMapping("/edit/{id}")
    HttpEntity<?> edit(@PathVariable Integer id,@RequestBody TableDTO tableDTO);

    @DeleteMapping("/delete/{id}")
    HttpEntity<?> delete(@PathVariable Integer id);
}
