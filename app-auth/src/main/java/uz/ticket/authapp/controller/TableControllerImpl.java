package uz.ticket.authapp.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import uz.ticket.authapp.payload.TableDTO;
import uz.ticket.authapp.service.TableService;

@RestController
@RequiredArgsConstructor
public class TableControllerImpl implements TableController{
    final TableService tableService;
    @Override
    public HttpEntity<?> add(TableDTO tableDTO) {
        return ResponseEntity.ok(tableService.add(tableDTO));
    }

    @Override
    public HttpEntity<?> get(Integer id) {
        return ResponseEntity.ok(tableService.get(id));
    }

    @Override
    public HttpEntity<?> getAll() {
        return ResponseEntity.ok(tableService.getAll());
    }

    @Override
    public HttpEntity<?> edit(Integer id, TableDTO tableDTO) {
        return ResponseEntity.ok(tableService.edit(id,tableDTO));
    }

    @Override
    public HttpEntity<?> delete(Integer id) {
        return ResponseEntity.ok(tableService.delete(id));
    }
}
