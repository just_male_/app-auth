package uz.ticket.authapp.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.Data;

import java.util.UUID;

@Entity
@Data
public class DependedAttachments {
    @Id
    private UUID uuid;
    private UUID attachmentId;
    private UUID tableId;
    private String purpose;
}
