package uz.ticket.authapp.entity;

import jakarta.persistence.*;
import lombok.Data;

import javax.swing.text.StyledEditorKit;
import java.util.List;

@Entity
@Data
public class Restaurant {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private String description;
    private String location;
    private Integer capacity;
    private Boolean isActive;
    @OneToMany
    private List<User> userList;
}
