package uz.ticket.authapp.entity;

import jakarta.persistence.*;
import lombok.Data;
import uz.ticket.authapp.entity.enums.Authorities;

import java.util.List;
import java.util.Set;

@Entity
@Data
@Table(name = "roles")
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String roleName;
    @ElementCollection
    @Enumerated(value = EnumType.STRING)
    private List<Authorities> authorities;
    private Boolean isDeleted;


}
