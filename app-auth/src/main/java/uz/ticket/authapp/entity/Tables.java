package uz.ticket.authapp.entity;

import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;

import java.sql.Timestamp;

@Entity
@Data
public class Tables {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private Integer number;
    private Integer capacity;
    @CreationTimestamp
    private Timestamp timestamp;
    private Boolean isBooked;
    private Double priceForTable;
    @ManyToOne
    private Restaurant restaurant;
    private Boolean isDeleted;

}
