package uz.ticket.authapp.entity;

import jakarta.persistence.*;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import uz.ticket.authapp.entity.enums.Authorities;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

@Entity
@Setter
@Getter
@ToString
@Table(name = "users")
public class User implements UserDetails {
    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "org.hibernate.id.UUIDGenerator")
    private UUID id;
    private String name;
    @Column(unique = true,nullable = false)
    private String email;
    private String password;
    private String code;
//    private String role;
    private UUID profilePhotoId;

//    @ManyToMany(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
//    private List<Authority> authorities;

    @ManyToOne(optional = false,fetch = FetchType.LAZY)
    private Role role;

    private Boolean accountNonExpired=true;
    private Boolean accountNonLocked=true;
    private Boolean credentialsNonExpired=true;
    private Boolean enabled=false;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<Authorities> authorities = role.getAuthorities();
        List<GrantedAuthority> authorityList=new ArrayList<>();
        for (Authorities authority : authorities) {
            authorityList.add(new SimpleGrantedAuthority(authority.name()));
        }
        return authorityList;
    }



    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return accountNonExpired;
    }

    @Override
    public boolean isAccountNonLocked() {
        return accountNonLocked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return credentialsNonExpired;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }
}
