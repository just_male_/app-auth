package uz.ticket.authapp.entity.enums;

public enum Authorities {
    EDIT_RESTAURANT,
    ADD_RESTAURANT,
    SHOW_RESTAURANT,
    DELETE_RESTAURANT,
    EDIT_TABLE,
    ADD_TABLE,
    SHOW_TABLE,
    DELETE_TABLE,
    BOOK_TABLE,
    CANCLE_BOOKING,
    CANCLE_OWN_BOOKING,
    ADD_ROLE,
    EDIT_ROLE,
    DELETE_ROLE,
    SHOW_ROLE
}
