package uz.ticket.authapp.exception.e;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import uz.ticket.authapp.exception.*;

import java.time.ZoneId;
import java.time.ZonedDateTime;

@ControllerAdvice
public class CustomExceptionHandler {
    @ExceptionHandler(value = {BadRequestException.class})
    public ResponseEntity<Object> handlerCustomException(BadRequestException badRequestException) {
        HttpStatus badRequest = HttpStatus.BAD_REQUEST;
        CustomException customException = new CustomException(
                badRequestException.getMessage(),
                badRequest,
                ZonedDateTime.now(ZoneId.of("Asia/Tashkent")));
        return new ResponseEntity<>(customException, badRequest);
    }

    @ExceptionHandler(value = {ForbiddenException.class})
    public ResponseEntity<Object> handlerCustomException(ForbiddenException forbiddenException) {
        HttpStatus forbidden = HttpStatus.FORBIDDEN;
        CustomException customException = new CustomException(
                forbiddenException.getMessage(),
                forbidden,
                ZonedDateTime.now(ZoneId.of("Asia/Tashkent")));
        return new ResponseEntity<>(customException, forbidden);
    }

    @ExceptionHandler(value = {ConflictException.class})
    public ResponseEntity<Object> handlerCustomException(ConflictException conflictException) {
        HttpStatus conflict = HttpStatus.CONFLICT;
        CustomException customException = new CustomException(
                conflictException.getMessage(),
                conflict,
                ZonedDateTime.now(ZoneId.of("Asia/Tashkent")));
        return new ResponseEntity<>(customException, conflict);
    }

    @ExceptionHandler(value = {NotFoundException.class})
    public ResponseEntity<Object> handlerCustomException(NotFoundException notFoundException) {
        HttpStatus notFound = HttpStatus.NOT_FOUND;
        CustomException customException = new CustomException(
                notFoundException.getMessage(),
                notFound,
                ZonedDateTime.now(ZoneId.of("Asia/Tashkent")));
        return new ResponseEntity<>(customException, notFound);
    }

    @ExceptionHandler(value = {UnauthorizedException.class})
    public ResponseEntity<Object> handlerCustomException(UnauthorizedException unauthorizedException) {
        HttpStatus unauthorized = HttpStatus.UNAUTHORIZED;
        CustomException customException = new CustomException(
                unauthorizedException.getMessage(),
                unauthorized,
                ZonedDateTime.now(ZoneId.of("Asia/Tashkent")));
        return new ResponseEntity<>(customException, unauthorized);
    }
}
