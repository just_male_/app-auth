package uz.ticket.authapp.filter;

import lombok.Data;

import java.util.Objects;

@Data
public class Context {
    private Object object;
}
