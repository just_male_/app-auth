package uz.ticket.authapp.filter;

import jakarta.servlet.*;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.apache.catalina.connector.RequestFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import uz.ticket.authapp.entity.Role;
import uz.ticket.authapp.entity.User;
import uz.ticket.authapp.repository.UserRepository;
import uz.ticket.authapp.service.RoleService;
import uz.ticket.authapp.util.Utills;

import java.io.IOException;
import java.util.Base64;
import java.util.Objects;
import java.util.Optional;

@Component
@RequiredArgsConstructor
public class MyFilter extends OncePerRequestFilter {
    final JwtProvider jwtProvider;
    final UserRepository userRepository;
    final RoleService roleService;
    @Autowired
    @Lazy
    PasswordEncoder passwordEncoder;

    @Autowired
    @Lazy
    UserDetailsService userDetailsService;




    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {

        String authorization = request.getHeader("authorization");
        if (Objects.isNull(authorization) || Objects.equals(authorization, "anonymousUser")) {
            filterChain.doFilter(request, response);
            return;
        }
        if (authorization.startsWith(Utills.BASIC_AUTH)) {
            Base64.Decoder decoder = Base64.getDecoder();

            authorization = authorization.substring(Utills.BASIC_AUTH.length());
            authorization= new String(decoder.decode(authorization));
            String[] split = authorization.split(":");
            String email = split[0];
            String password = split[1];

            User user = (User) userDetailsService.loadUserByUsername(email);
            if (passwordEncoder.matches(password,user.getPassword()))
            SecurityContextHolder.getContext().setAuthentication(
                    new UsernamePasswordAuthenticationToken(
                            user,
                            null,
                            user.getAuthorities()
                    )
            );
        }
        if (authorization.startsWith(Utills.BEARER_AUTH)) {
            authorization = authorization.substring(Utills.BEARER_AUTH.length());
            String emailFromToken = jwtProvider.getEmailFromToken(authorization);
            User user = (User) userDetailsService.loadUserByUsername(emailFromToken);
            SecurityContextHolder.getContext().setAuthentication(
                    new UsernamePasswordAuthenticationToken(
                            user,
                            null,
                            user.getAuthorities()
                    )
            );

        }
        filterChain.doFilter(request, response);
    }
}
