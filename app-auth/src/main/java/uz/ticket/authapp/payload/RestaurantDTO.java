package uz.ticket.authapp.payload;

import lombok.Data;
import uz.ticket.authapp.entity.User;

import java.util.List;

@Data
public class RestaurantDTO {
    private String name;
    private String description;
    private String location;
    private Integer capacity;
    private List<User> userList;
}
