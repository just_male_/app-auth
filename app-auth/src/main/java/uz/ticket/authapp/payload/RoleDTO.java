package uz.ticket.authapp.payload;

import lombok.Data;
import uz.ticket.authapp.entity.enums.Authorities;

import java.util.Set;

@Data
public class RoleDTO {
    private String roleName;
    private Authorities[] authorities;
}
