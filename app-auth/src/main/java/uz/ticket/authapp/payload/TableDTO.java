package uz.ticket.authapp.payload;

import lombok.Data;
import uz.ticket.authapp.entity.Restaurant;

@Data
public class TableDTO {
    private Integer number;
    private Integer capacity;
    private Restaurant restaurant;
    private Double priceForTable;
    private Boolean isBooked;
}
