package uz.ticket.authapp.payload;

import lombok.Data;

@Data
public class UserDTO {
    private String  name;
    private String  email;
    private String  password;
}
