package uz.ticket.authapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.ticket.authapp.entity.Restaurant;

public interface RestaurantRepository extends JpaRepository<Restaurant,Integer> {
}
