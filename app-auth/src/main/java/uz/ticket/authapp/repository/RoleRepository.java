package uz.ticket.authapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.ticket.authapp.entity.Role;

import java.util.Optional;

public interface RoleRepository  extends JpaRepository<Role,Short> {
    Optional<Role> findById(Integer roleId);
    Optional<Role> findByRoleName(String roleName);
}

