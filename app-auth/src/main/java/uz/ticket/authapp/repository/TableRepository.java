package uz.ticket.authapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.ticket.authapp.entity.Tables;
import uz.ticket.authapp.entity.User;

import java.util.List;
import java.util.Optional;

public interface TableRepository extends JpaRepository<Tables,Integer> {
    List<Tables> findAllByIsDeletedFalse();
}
