package uz.ticket.authapp.service;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class EmailDetails {

    // Class data members
    private String recipient;
    private String msgBody;
    private String subject;
    private String attachment;
}