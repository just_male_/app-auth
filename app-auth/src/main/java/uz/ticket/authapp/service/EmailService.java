package uz.ticket.authapp.service;


import uz.ticket.authapp.payload.ResultMessage;


public interface EmailService {
    // Method
    // To send a simple email
    ResultMessage sendSimpleMail(EmailDetails details);

    // Method
    // To send an email with attachment
    ResultMessage sendMailWithAttachment(EmailDetails details);
}
