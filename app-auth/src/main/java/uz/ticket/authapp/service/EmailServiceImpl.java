package uz.ticket.authapp.service;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import uz.ticket.authapp.payload.ResultMessage;

@Service
@RequiredArgsConstructor
public class EmailServiceImpl implements EmailService{

    final JavaMailSender javaMailSender;

    @Value("${spring.mail.username}")
    private String email;
    @Override
    public ResultMessage sendSimpleMail(EmailDetails details) {
        SimpleMailMessage mailMessage=new SimpleMailMessage();
        mailMessage.setFrom(email);
        mailMessage.setTo(details.getRecipient());
        mailMessage.setSubject(details.getSubject());
        mailMessage.setText(details.getMsgBody());
        try {
            javaMailSender.send(mailMessage);
        }catch (Exception e){
            return new ResultMessage(false,"something went wrong with email");
        }
        return new ResultMessage(true,"coonfirm your email");
    }

    @Override
    public ResultMessage sendMailWithAttachment(EmailDetails details) {
        return null;
    }
}
