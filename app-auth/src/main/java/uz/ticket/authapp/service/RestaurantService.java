package uz.ticket.authapp.service;

import org.springframework.stereotype.Service;
import uz.ticket.authapp.entity.Restaurant;
import uz.ticket.authapp.payload.RestaurantDTO;
import uz.ticket.authapp.payload.ResultMessage;

import java.util.List;

public interface RestaurantService {
    ResultMessage add(RestaurantDTO restaurantDTO);
    ResultMessage show(Integer id);
    List<Restaurant> showAll();
    ResultMessage edit(Integer id, RestaurantDTO restaurantDTO);
    ResultMessage delete(Integer id);
}
