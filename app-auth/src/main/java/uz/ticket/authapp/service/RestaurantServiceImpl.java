package uz.ticket.authapp.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import uz.ticket.authapp.entity.Restaurant;
import uz.ticket.authapp.entity.User;
import uz.ticket.authapp.payload.RestaurantDTO;
import uz.ticket.authapp.payload.ResultMessage;
import uz.ticket.authapp.repository.RestaurantRepository;
import uz.ticket.authapp.repository.UserRepository;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class RestaurantServiceImpl implements RestaurantService {

    final RestaurantRepository restaurantRepository;

    final UserRepository userRepository;

    @Override
    public ResultMessage add(RestaurantDTO restaurantDTO) {
        Restaurant checkRestaurant = checkRestaurant(restaurantDTO.getName());
        if (Objects.nonNull(checkRestaurant)) {
            return new ResultMessage(false, "Restaurant added before!!!");
        }
        for (User user: restaurantDTO.getUserList()) {
            Optional<User> optionalUser = userRepository.findById(user.getId());
            if (optionalUser.isEmpty()) return
                    new ResultMessage(false,"Cannot find worker with id " + user.getId());
        }

        Restaurant restaurant = new Restaurant();
        restaurant.setName(restaurantDTO.getName());
        restaurant.setLocation(restaurantDTO.getLocation());
        restaurant.setCapacity(restaurantDTO.getCapacity());
        restaurant.setDescription(restaurantDTO.getDescription());
        restaurant.setIsActive(true);
        restaurant.setUserList(restaurantDTO.getUserList());

        restaurantRepository.save(restaurant);
        return new ResultMessage(true, restaurant);
    }

    private Restaurant checkRestaurant(String restaurantName) {
        List<Restaurant> restaurants = restaurantRepository.findAll();
        for (Restaurant rest:restaurants) {
            if(rest.getName().equals(restaurantName))
            {
                return rest;
            }
        }
        return null;
    }

    @Override
    public ResultMessage show(Integer id) {
        Optional<Restaurant> optionalRestaurant = restaurantRepository.findById(id);
        if (optionalRestaurant.isEmpty()) return new ResultMessage(false, "restaurant not found");

        return new ResultMessage(true, optionalRestaurant.get());
    }

    @Override
    public List<Restaurant> showAll() {
        return restaurantRepository.findAll();
    }

    @Override
    public ResultMessage edit(Integer id, RestaurantDTO restaurantDTO) {
        Optional<Restaurant> optionalRestaurant = restaurantRepository.findById(id);
        if (optionalRestaurant.isEmpty()) return
                new ResultMessage(false, "Cannot find restaurant with id " + id);

        for (User user: restaurantDTO.getUserList()) {
            Optional<User> optionalUser = userRepository.findById(user.getId());
            if (optionalUser.isEmpty()) return
                    new ResultMessage(false,"Cannot find worker with id " + user.getId());
        }
        Restaurant restaurant1 = optionalRestaurant.get();
        restaurant1.setName(restaurantDTO.getName());
        restaurant1.setDescription(restaurantDTO.getDescription());
        restaurant1.setLocation(restaurantDTO.getLocation());
        restaurant1.setCapacity(restaurantDTO.getCapacity());
        restaurant1.setUserList(restaurantDTO.getUserList());
        restaurant1.setIsActive(true);
        restaurantRepository.save(restaurant1);
        return new ResultMessage(true, restaurant1);
    }

    @Override
    public ResultMessage delete(Integer id) {
        Optional<Restaurant> optionalRestaurant = restaurantRepository.findById(id);

        if (optionalRestaurant.isPresent()) {
            Restaurant restaurant = optionalRestaurant.get();
            restaurant.setIsActive(false);
            restaurantRepository.save(restaurant);
            return new ResultMessage(true, restaurant);
        }
        return new ResultMessage(false, "Restaurant not found");
    }
}
