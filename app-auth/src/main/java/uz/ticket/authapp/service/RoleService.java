package uz.ticket.authapp.service;

import uz.ticket.authapp.entity.User;
import uz.ticket.authapp.payload.ResultMessage;
import uz.ticket.authapp.payload.RoleDTO;

public interface RoleService {
    ResultMessage add(RoleDTO roleDTO);
    ResultMessage get(Integer roleId);
    ResultMessage edit(Integer id,RoleDTO roleDTO);
    ResultMessage getAll();
    ResultMessage delete(Integer id);
}
