package uz.ticket.authapp.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import uz.ticket.authapp.entity.Role;
import uz.ticket.authapp.entity.User;
import uz.ticket.authapp.entity.enums.Authorities;
import uz.ticket.authapp.payload.ResultMessage;
import uz.ticket.authapp.payload.RoleDTO;
import uz.ticket.authapp.payload.UserDTO;
import uz.ticket.authapp.repository.RoleRepository;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class RoleServiceImpl implements RoleService{
    final RoleRepository roleRepository;

    public ResultMessage add(RoleDTO roleDTO)
    {
        if(checkRole(roleDTO))
        {
            Role role = new Role();
            role.setRoleName(roleDTO.getRoleName());
            role.setAuthorities( Arrays.asList(roleDTO.getAuthorities()));
            role.setIsDeleted(false);
            roleRepository.save(role);
            return new ResultMessage(true,role);
        }
        return new ResultMessage(false,"already exist");
    }

    public boolean checkRole(RoleDTO roleDTO)
    {
        List<Role> roles = roleRepository.findAll();
        for (Role role:roles) {
            if(role.getRoleName().equals(roleDTO.getRoleName()))
            {
                return false;
            }
        }
        return true;
    }
    public ResultMessage get(Integer roleId)
    {
        Optional<Role> byId = roleRepository.findById(roleId);
        if(byId.isPresent())
        {
            return new ResultMessage(true,byId.get());
        }
        return new ResultMessage(false,"not found");
    }
    public ResultMessage edit(Integer id,RoleDTO roleDTO)
    {
        Optional<Role> byId = roleRepository.findById(id);
        if (byId.isPresent())
        {
            Role role = byId.get();
            role.setRoleName(roleDTO.getRoleName());
            role.setAuthorities(Arrays.stream(roleDTO.getAuthorities()).toList());
            return new ResultMessage(true,"success");
        }
        return new ResultMessage(false,"smth went wrong");
    }
    public ResultMessage getAll()
    {
        List<Role> all = roleRepository.findAll();
        return new ResultMessage(true,all);
    }

    public ResultMessage delete(Integer id)
    {
        Optional<Role> byId = roleRepository.findById(id);
        if(byId.isPresent())
        {
            byId.get().setIsDeleted(true);
            return new ResultMessage(true,"deleted");
        }
        return new ResultMessage(false,"smth went wrong");
    }



}
