package uz.ticket.authapp.service;

import org.springframework.stereotype.Service;
import uz.ticket.authapp.payload.ResultMessage;
import uz.ticket.authapp.payload.TableDTO;

public interface TableService {
    ResultMessage add(TableDTO tableDTO);
    ResultMessage get(Integer id);
    ResultMessage getAll();
    ResultMessage edit(Integer id,TableDTO tableDTO);
    ResultMessage delete(Integer id);
}
