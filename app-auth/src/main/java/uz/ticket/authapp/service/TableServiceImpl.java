package uz.ticket.authapp.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import uz.ticket.authapp.entity.Tables;
import uz.ticket.authapp.payload.ResultMessage;
import uz.ticket.authapp.payload.TableDTO;
import uz.ticket.authapp.repository.TableRepository;

import java.sql.Time;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class TableServiceImpl implements TableService{
    final TableRepository tableRepository;

    @Override
    public ResultMessage add(TableDTO tableDTO) {
        if(checkTable(tableDTO))
        {
            Tables table = new Tables();
            table.setCapacity(tableDTO.getCapacity());
            table.setPriceForTable(tableDTO.getPriceForTable());
            table.setNumber(tableDTO.getNumber());
            table.setRestaurant(tableDTO.getRestaurant());
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            table.setTimestamp(timestamp);
            table.setIsBooked(tableDTO.getIsBooked());
            table.setIsDeleted(false);
            tableRepository.save(table);
            return new ResultMessage(true,"added");
        }
        return new ResultMessage(false,"already exist");
    }

    @Override
    public ResultMessage get(Integer id) {
        Optional<Tables> byId = tableRepository.findById(id);
        if(byId.isPresent())
        {
            return new ResultMessage(true,byId.get());
        }
        return new ResultMessage(false,"not found");
    }

    public boolean checkTable(TableDTO tableDTO)
    {
        List<Tables> tables = tableRepository.findAll();
        for (Tables table:tables) {
            if(table.getNumber().equals(tableDTO.getNumber()))
            {
                return false;
            }
        }
        return true;
    }
    @Override
    public ResultMessage getAll() {
        return new ResultMessage(true,tableRepository.findAllByIsDeletedFalse());
    }

    @Override
    public ResultMessage edit(Integer id,TableDTO tableDTO) {
        Optional<Tables> byId = tableRepository.findById(id);
        if(byId.isPresent())
        {
            Tables tables = byId.get();
            tables.setIsBooked(tableDTO.getIsBooked());
            tables.setNumber(tableDTO.getNumber());
            tables.setPriceForTable(tableDTO.getPriceForTable());
            tables.setRestaurant(tableDTO.getRestaurant());
            tables.setCapacity(tableDTO.getCapacity());
            tableRepository.save(tables);
            return new ResultMessage(true,"edited");
        }
        return new ResultMessage(false,"not found");
    }

    @Override
    public ResultMessage delete(Integer id) {
        Optional<Tables> byId = tableRepository.findById(id);
        if(byId.isPresent())
        {
            byId.get().setIsDeleted(true);
            return new ResultMessage(true,"deleted");
        }
        return new ResultMessage(false,"not found");
    }
}
